from feature import app, db
from flask import render_template, redirect, url_for, flash
from flask import get_flashed_messages
from feature.models import User
from feature.forms import RegisterForm, LoginForm
from flask_login import login_user, logout_user


@app.route("/")
@app.route("/home")
def home_page():
    return render_template("home.html")


@app.route("/register", methods=["GET", "POST"])
def register_page():
    form = RegisterForm()
    if form.validate_on_submit():
        user_to_create = User(
            mobile_number=form.mobile_number.data,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            birth_date=form.birth_date.data.strftime("%Y-%m-%d"),
            gender=form.gender.data,
            email_address=form.email_address.data,
        )
        db.session.add(user_to_create)
        db.session.commit()
        redirect(url_for("login_page"))
    if form.errors != {}:  # if there are not errors from the validation
        for err_msg in form.errors.values():
            flash(
                f"There was an error ith creating the user: {err_msg}",
                category="danger",
            )
    return render_template("register.html", form=form)


@app.route("/login", methods=["GET", "POST"])
def login_page():
    return render_template("login.html")


@app.route("/logout")
def logout_page():
    logout_user()
    flash("You have been logged out!", category="info")
    return redirect(url_for("home_page"))
