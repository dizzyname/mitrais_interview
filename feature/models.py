from feature import db, login_manager


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    mobile_number = db.Column(db.String(length=15), unique=True, nullable=False)
    first_name = db.Column(db.String(length=30), unique=False, nullable=False)
    last_name = db.Column(db.String(length=30), unique=False, nullable=False)
    birth_date = db.Column(db.String(length=15), unique=False, nullable=True)
    gender = db.Column(db.String(length=8), unique=False, nullable=True)
    email_address = db.Column(db.String(length=50), unique=True, nullable=False)
