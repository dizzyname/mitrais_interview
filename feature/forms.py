from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, RadioField, DateField
from wtforms.validators import Length, EqualTo, Email, DataRequired, ValidationError
from feature.models import User


class RegisterForm(FlaskForm):
    def validate_mobile_number(self, mobile_number_to_check):
        mobile_number = User.query.filter_by(
            mobile_number=mobile_number_to_check.data
        ).first()
        if mobile_number:
            raise ValidationError(
                "Mobile Number already exists! Please try a different mobile number"
            )

    def validate_email_address(self, email_address_to_check):
        email_address = User.query.filter_by(
            email_address=email_address_to_check.data
        ).first()
        if email_address:
            raise ValidationError(
                "Email address already exists! Please try a different email address"
            )

    mobile_number = StringField(
        label="Mobile Number", validators=[Length(max=15), DataRequired()]
    )
    first_name = StringField(
        label="First Name: ", validators=[Length(min=2, max=30), DataRequired()]
    )
    last_name = StringField(
        label="Last Name: ", validators=[Length(min=2, max=30), DataRequired()]
    )
    birth_date = DateField("Date of Birth")
    gender = RadioField(label="Gender", choices=["Male", "Female"])
    email_address = StringField(
        label="Email Address: ", validators=[Email(), DataRequired()]
    )
    submit = SubmitField(label="Register")


class LoginForm(FlaskForm):
    username = StringField(label="User Name: ", validators=[DataRequired()])
    password = PasswordField(label="Password: ", validators=[DataRequired()])
    submit = SubmitField(label="Sign In")
